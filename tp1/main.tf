resource "aws_key_pair" "deployer" {
  key_name = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwqlTgLi/GdoYs+yF1hVrnP5Ds3VB45Lofvc8KjAPho0/uDclEDpCSFRW9CKra2eVpXyhNICsu9fiwkexC1XwZy7X4uIq7q7NYe/bghOVYlCWwz7cLV06bfPTrJbBSYXgmdYpKB6dPYVyDV5vOyzSfEyg0WgCZeIOjCwKQsEx5lu7EoCfcEYInWAmvWPnN32hUOvnSeLMLm+1Ct0GEyTvH/taRhYWyK4nB4lcp4mHuXEWBASoLYz7BuLKC3HGkwXwMSLSQZzDy2Tf+v0TMiAto8iTyvDD8JaNwt1XoF2GrGzCP7vpOssQYjuVYav6Nm5IyET10E85QHbkoKZUjy9fuLTd173B0fQdqy1jlWPCx1e4QCzamt9AfSZnEhSx4KBMCjDjGMGVdEwycCnjX0x/oewS2Iu3z6+ZfvraJUlGnwjqDRlJQo5QT3IRBReSZxuYhVnretY207JLzfLEeK8UOt8MSDxonxib2HufAyN9mqsXs9tRffeeeLm9eOnUvlzc= cecilekafrouni@ubuntu"
}

resource "aws_default_vpc" "vpc" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "security_group" {
  vpc_id = aws_default_vpc.vpc.id

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


provider "aws" {
  region = "eu-west-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.deployer.key_name

  tags = {
    Name = "HelloWorld"
  }

}

output "output_IP" {
  value = aws_instance.web.public_ip
}